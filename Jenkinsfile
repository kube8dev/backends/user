def LABEL_ID = "kube8-${UUID.randomUUID().toString()}"

podTemplate(
    label: LABEL_ID, 
    containers: [
        containerTemplate(args: 'cat', name: 'docker', command: '/bin/sh -c', image: 'docker', ttyEnabled: true),
        containerTemplate(args: 'cat', name: 'helm', command: '/bin/sh -c', image: 'lachlanevenson/k8s-helm:v2.11.0', ttyEnabled: true)
    ],
    volumes: [
      hostPathVolume(mountPath: '/var/run/docker.sock', hostPath: '/var/run/docker.sock')
    ]
){
    // Start pipeline
    node(LABEL_ID) {
        def REPOS
        def IMAGE_VERSION
        def IMAGE_POSFIX = ""
		def KUBE_NAMESPACE
        def ENVIRONMENT
        def IMAGE_NAME = "kube8-backend-user"
		def GIT_BRANCH 
		def HELM_DEPLOY_NAME
		def HELM_CHART_NAME = "kube8/backend-user"
        def CHARTMUSEUM_REPO_URL = "http://helm-chartmuseum:8080"
        def INGRESS_HOST = "dev.becaster.com.br"

        stage('Checkout') {
            echo 'Iniciando clone do repositorio'
			REPOS = checkout scm
            
			GIT_BRANCH = REPOS.GIT_BRANCH

			echo GIT_BRANCH

			if(GIT_BRANCH.equals('master')){
				KUBE_NAMESPACE = "prod"
				ENVIRONMENT = "production"
			} else if(GIT_BRANCH.equals('develop')){
				KUBE_NAMESPACE = "staging"
				ENVIRONMENT = "staging"
                IMAGE_POSFIX = "-rc"
                INGRESS_HOST = "staging.dev.becaster.com.br"
			} else {
				def error = "Não existe pipeline para a branch ${GIT_BRANCH}"
				throw new Exception(error) 
			}

			HELM_DEPLOY_NAME = "kube8-${KUBE_NAMESPACE}-backend-user"
			
			IMAGE_VERSION = sh returnStdout: true, script: 'sh read-package-version.sh'
            IMAGE_VERSION = IMAGE_VERSION.trim() + IMAGE_POSFIX
            echo IMAGE_VERSION
        }
        stage("Package") {
            container('docker') {
                echo 'Iniciando empacotamento com Docker'
                withCredentials([usernamePassword(credentialsId: 'docker', passwordVariable: 'DOCKER_HUB_PSSWD', usernameVariable: 'DOCKER_HUB_USER')]) {
                    sh "echo '${DOCKER_HUB_PSSWD}' | docker login --username ${DOCKER_HUB_USER} --password-stdin"
                    sh 'ls'
                    sh "docker build -t ${DOCKER_HUB_USER}/${IMAGE_NAME}:${IMAGE_VERSION} ."
                    sh "docker push ${DOCKER_HUB_USER}/${IMAGE_NAME}:${IMAGE_VERSION}"
                }
            }
        }
        stage("Deploy"){
            container("helm") {
                echo 'Iniciando deploy com helm'
                sh "helm init --client-only"
                sh "helm repo add kube8 ${CHARTMUSEUM_REPO_URL}"
                sh "helm repo list"
                sh "helm repo update"
				try {
                	sh "helm upgrade --namespace=${KUBE_NAMESPACE} ${HELM_DEPLOY_NAME} ${HELM_CHART_NAME} --set image.tag=${IMAGE_VERSION} --set ingress.hosts[0]=${INGRESS_HOST}"
				} catch(Exception ex) {
                	sh "helm install --namespace=${KUBE_NAMESPACE} --name ${HELM_DEPLOY_NAME} ${HELM_CHART_NAME} --set image.tag=${IMAGE_VERSION} --set ingress.hosts[0]=${INGRESS_HOST}"					
				}
			}
        }
    }
}